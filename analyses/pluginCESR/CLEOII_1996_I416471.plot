BEGIN PLOT /CLEOII_1996_I416471/d02-x01-y01
Title=Spectrum for $\Xi_c^{*+}$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
