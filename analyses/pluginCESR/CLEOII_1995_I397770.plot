BEGIN PLOT /CLEOII_1995_I397770/d02-x01-y01
Title=Spectrum for $\Xi_c^{*0}$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
