BEGIN PLOT /BESIII_2018_I1697371/d01-x01-y01
Title=$D^0\to K^- \mu^+ \nu_\mu$
XLabel=$q^2/$GeV$^2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}q^2$ [$\text{GeV}^{-2}$]
END PLOT
